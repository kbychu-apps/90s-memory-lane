# 90s Memory Lane

Ah, the 90s!

For those of us old enough to have lived through the 90s, we certainly have fond (and likely embarrassing) memories. This project is meant to be a simple social sharing site where we can have a collective chuckle at all things 90s.

# Requirements

The original intent of this project is to have a live application with traffic so that we can more effectively dogfood GitLab. As such, there are some specific requirements.
* Composed of micro-services
* Different deployment model for different services
* Uses Kubernetes
* Be interesting enough that real users will actually want to visit the site :) So that we can monitor it, respond to incidents, etc.